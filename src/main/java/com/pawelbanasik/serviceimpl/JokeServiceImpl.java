package com.pawelbanasik.serviceimpl;

import org.springframework.stereotype.Service;

import com.pawelbanasik.service.JokeService;

import guru.springframework.norris.chuck.ChuckNorrisQuotes;

@Service
public class JokeServiceImpl implements JokeService {

	private final ChuckNorrisQuotes chuckNorrisQuotes;

	public JokeServiceImpl(ChuckNorrisQuotes chuckNorrisQuotes) {
		this.chuckNorrisQuotes = chuckNorrisQuotes;
	}

	@Override
	public String getRandomQuotes() {
		// I would do it like this (also works)
		// ChuckNorrisQuotes chuckNorrisQuotes = new ChuckNorrisQuotes();
		return chuckNorrisQuotes.getRandomQuote();
	}

}
